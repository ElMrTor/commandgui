# Command Menu GUI

## Requirements
- Python 3 must be installed

## How to run
- Start by going into the folder main folder of the app where you can see the CommandMenuClient.py file.
- From here the program will should start.

>>>
Example:

python3 CommandMenuClient.py

python CommandMenuClient.py
>>>