from tkinter import N, S, E, W, Tk, Menu, Menubutton, Button, Text, Toplevel, Frame, GROOVE, Label, RAISED, END, Entry, PhotoImage, Scrollbar, Canvas, ALL, Radiobutton, StringVar
from tkinter.ttk import Combobox
from typing import List, Dict, Tuple, Union
from sys import exit
from os import getcwd
from sys import platform
from socket import socket, errno, timeout
from threading import Thread
from re import compile

# Depending on the version of python 3, re has Pattern class, in others it doesn't
try:        
	from re import Pattern
except ImportError:
	Pattern = type(compile(''))


DEFAULT_ADDRESS = '192.168.0.15'
DEFAULT_PORT = 1234

DEFAULT_BORDER = GROOVE
BAD_COMMAND = '_BAD_'

def read_file(path_to_file: str) -> list:
    ''' Reads a file and returns its contents as a list of strings. '''
    file_content: list
    try:
        with open(path_to_file, 'r') as current_file:
            file_content = current_file.readlines()
    except:
        print('Could not read the specified file.')
        raise
    return file_content


class CommandMenuGUI:
    ''' Main class, runs the user interface. '''
    def __init__(self):
        self.main_window = Tk() # Root instance of the interface
        self.main_menu_bar =  Menu(self.main_window) # The main menu bar where every menu instance goes to
        self.ip_settings_frame = IPSettingsWindow(self.main_window) #Instance for IP address settings
        self.ip_settings_frame.set_command_menu_instance(self) # Pass this instance to the settings frame so that it can change theh addresses directly
        self.button_frame_handler: List = [] # For now the one that holds the button and the addresses that are gonna be used. Probably gonna change this later.
        self.scrollable_command_frame :ScrollableFrame = None
        self.server_address = DEFAULT_ADDRESS
        self.server_port = DEFAULT_PORT
        self._initialize_user_interface()
        
        self.main_window.mainloop() # Starts the usr interface, withouth this method nothing would run.
    

    def _initialize_user_interface(self) -> None:
        main_w = self.main_window
        main_w.title('Command Menu')
        main_w.geometry('600x600')
        main_w.wm_resizable(0,0) # Restrict resize on the window.
        main_w.columnconfigure(0, weight = 1) # At the moment only one column of the main window is being used, weight is so that components inside can expand.
        main_w.rowconfigure(0, weight = 1)
        if platform == 'win32':            
            program_icon = PhotoImage(file = getcwd() + r'\resources\Logo-RUMarino.gif')
        else:
            program_icon = PhotoImage(file = getcwd() + '/resources/Logo-RUMarino.gif') # Sets the program icon.        
        main_w.iconphoto(True, program_icon) # Every child window will inherit the icon.        
        
        

        self.scrollable_command_frame = ScrollableFrame(main_w)
        self.scrollable_command_frame.grid(row = 0, column = 0, sticky = N+E+W+S)
        f_creator = CommandFrameCreator(self.scrollable_command_frame)
        f_creator.check_for_command()
        # self.scrollable_command_frame.add_frame(ComplexButtonFrame(self.scrollable_command_frame.content_frame, 'Testing', 'ts', ['Param1', 'Param2', 'Param3', 'Param4', 'Param5', 'Param6', 'Param7']))
        
        self._create_menu_items()
        self.main_window.after(100, self._check_new_server_address) # Schedule check if the ip address changed. Set to one second after the program starts.
        
    def _create_menu_items(self) -> None:
        ''' Creates and assembles the menu icons that are going to be in the main or root window.'''
        settings_menu = Menu(self.main_menu_bar, tearoff = 0)
        settings_menu.add_command(label = 'IP Settings', command = self.ip_settings_frame.hide_window)
        self.main_menu_bar.add_cascade(label = 'Settings', menu = settings_menu)

        # Add the main menubar to the main window
        self.main_window.config(menu = self.main_menu_bar)


    def _check_new_server_address(self) -> None:
        ''' Cheks if the ip address or port changegd, if so assigns the new value to the button frame instances, each one has its ownn individual address...'''        
        if self.scrollable_command_frame:
            self.scrollable_command_frame.update_frame_addresses(str(self.server_address), str(self.server_port))        
        self.main_window.after(500, self._check_new_server_address)
        

    def read_button_config(self) -> List[str]:
        ''' Reads file and returns the contents as list. '''
        return read_file(getcwd() + '/resources/btn_config')



class TopWindow(Toplevel):
    ''' Useful class for creating TopLevelWindow that has some small settings set that are useful. '''
    def __init__(self, args):
        super(TopWindow, self).__init__(args)        
        self.attributes('-topmost', 'true') # Window will always be on top no matter what.
        self.protocol('WM_DELETE_WINDOW', self.hide_window) # Sets the X button function to hide the window so it doesn't terminate the program.
        self.hide_window() # Called so that the window doesn't appear when the program starts.

    def hide_window(self) -> None:
        ''' Hides the window or makes it visible depending on the state of the top level window. '''
        if self.state() == 'normal':
            self.withdraw()
        else:
            self.deiconify()


class IPSettingsWindow(TopWindow):
    
    ''' Top Level Window used too set any changes of address and/or port of the server. '''

    def __init__(self, args):
        super(IPSettingsWindow, self).__init__(args)
        self.main_command_menu_instance: CommandMenuGUI = None
        self.ip_text_field: Entry
        self.port_txt_field: Entry
        self.new_addres = DEFAULT_ADDRESS
        self.new_port = DEFAULT_PORT
        self.window_components = [] # Every component, button, field, or label will be added here
        self._create_window()
    
    
    def get_new_ip_address(self):
        return self.new_addres

    def get_new_port(self):
        return self.new_port

    def set_command_menu_instance(self, cm) -> None:
        ''' Sets a command menu instance to make it easier to assign settings to the command menu. '''
        self.main_command_menu_instance = cm

    def _set_remote_address(self) -> None:
        '''
        Sets the new address and/or port if there was any change in them when called, it compares the address that is already stored and compares
        it to the text on the text fields.
        '''
        # TODO Probably could add some regex for field verification.
        if not self.main_command_menu_instance:
            raise AttributeError('Did not set the main command menu instance to be able to use this method.')
        ip_ad = self.ip_text_field.get()
        # print(ip_ad)
        port_ad = self.port_txt_field.get()
        # print(port_ad)
        if ip_ad and ip_ad != str(self.new_addres):            
            self.new_addres = ip_ad
            self.main_command_menu_instance.server_address = ip_ad            
        if port_ad and port_ad != str(self.new_port):            
            self.new_port = port_ad
            self.main_command_menu_instance.server_port = port_ad

    def _create_window(self) -> None:
        self.resizable(0,0) # Restrict window resize.
        self.geometry('500x150')
        self.title('IP Settings')
        
        # Makes it so that every column and row internal components can expand.
        self.rowconfigure(0, weight = 3)
        self.rowconfigure(1, weight = 3)
        self.rowconfigure(2, weight = 1)
        self.columnconfigure(0, weight = 1)
        self.columnconfigure(1, weight = 1)
        
        
        # Labels in the window
        ip_address_lbl = Label(self, text = 'IP Address:')
        ip_address_lbl.grid(row = 0, column = 0, sticky = N+E+W+S)
        port_lbl = Label(self, text = 'Port Number:')
        port_lbl.grid(row = 1, column = 0, sticky = N+E+W+S)

        # Text field in the window
        ip_text_field = Entry(self)
        ip_text_field.insert(END, self.new_addres)
        ip_text_field.grid(row = 0, column  = 1, sticky = N+E+W+S)
        port_txt_field = Entry(self)
        port_txt_field.insert(END, self.new_port)
        port_txt_field.grid(row = 1, column = 1, sticky = N+E+W+S)

        # Submit button        
        submit_btn = Button(self, text = 'Save', command = self._set_remote_address)
        submit_btn.grid(row = 2, column = 0, sticky = N+E+W+S, columnspan = 2)

        # TODO Marroneo, fix later or not...
        self.ip_text_field = ip_text_field
        self.port_txt_field = port_txt_field

        # Add every component to the list to keep track of it
        # Suggestion to just add them as instance variables
        self.window_components.append(ip_address_lbl)
        self.window_components.append(port_lbl)
        self.window_components.append(ip_text_field)
        self.window_components.append(port_txt_field)
        self.window_components.append(submit_btn)


class CommandButtonFrame(Frame):
    ''' Class used as template, so its easier to create frames with buttons and commands. Used as parent class, does very little in its own. '''
    def __init__(self, parent_frame):
        super(CommandButtonFrame, self).__init__(parent_frame)
        self.config(relief = RAISED, bd = 2)
        self.server_address = DEFAULT_ADDRESS
        self.server_port = DEFAULT_PORT        
        self.server_msg: str = None
        self.error_lbl = None

        # Not being used atm
        self.command_regex = None
        # Not being used atm
        
        
    def add_error_lbl(self):
        ''' Sets the error label instance to the command button '''
        self.error_lbl = ErrorLabel(self)

    def set_command_regex(self, regex: str):
        ''' Sets a regex specific to this command button, making it easier to make input verification. '''
        self.command_regex = compile(regex)

    def set_server_message(self, message: str) -> None:
        ''' Method that facilitates setting the message that will be sent to the server. '''
        self.server_msg = message

    def set_bad_command_msg(self, msg: str)-> str:
        ''' Makes it easier to indicate the current command is bad, used to verify the message before sending it in some command buttons. '''
        return BAD_COMMAND + msg
        
    def send_to_server(self, host_address: str, host_port: int, message: str) -> None:
        # TODO This could be refactored into something better.
        # TODO I don't think this handles correctly when the messages are None
        ''' Method must be implemented in a child class. '''

        # If there is no message set and no error label it will raise an exception.
        if not self.server_msg and not self.error_lbl:
            raise NotImplementedError('Method not implemented.')

        # If there is a bad message set and an error label it will pass the corresponding message to the error label to be displayed in the gui.
        elif self.error_lbl and self.server_msg and len(self.server_msg) >= len(BAD_COMMAND) and self.server_msg[:5] == BAD_COMMAND:
            self.error_lbl.set_message(self.server_msg[len(BAD_COMMAND):])
        
        # elif self.error_lbl and self.server_msg:
        #     if not self.command_regex.fullmatch(self.server_msg)
        else:
            connection = SocketManager(address=self.server_address, port=self.server_port)
            t = Thread(target = connection.connect_and_send_msg_to_server,args = [self.server_msg])
            t.start()

            # Unlink connection and thread instance so the garbage collector hopefully disposes of them.            
            connection = None
            t = None

    def set_server_port_and_address(self, server_address, server_port) -> None:
        self.server_address = server_address
        self.server_port= server_port

    def report_error(self, error: Union[Exception, str]):
        ''' Method used for exceptions, if there is an active error label it is passed on it instead of being raised. '''
        if self.error_lbl:
            self.error_lbl.set_message(error)
        elif isinstance(error, Exception):
            raise error

    
    def auto_add_send_button(self):
        ''' Automatically adds the send button at the end of the commandbutton frame. '''
        if self.error_lbl:
            self.error_lbl.grid(row = len(self.winfo_children()), column = 0, sticky = N+E+W+S)            
        send_btn = Button(self, text = 'Send', command = lambda: self.send_to_server(self.server_address, self.server_port, self.server_msg))
        send_btn.grid(row = len(self.winfo_children()), column = 0, sticky = N+E+W+S)
        



class SimpleCommandButtonFrame(CommandButtonFrame):
    ''' 
    Class that makes it easy to add a frame with a label with description or name, send button that will send the string that is given
    when the class is created (button_send_text).
    '''
    def __init__(self, parent_frame, button_name, button_send_text):
        super(SimpleCommandButtonFrame, self).__init__(parent_frame)
        self.button_name = button_name
        self.button_send_text = button_send_text
        self.server_address: str = DEFAULT_ADDRESS
        self.server_port: str = DEFAULT_PORT
        self.rowconfigure(0, weight = 1)
        self.rowconfigure(1, weight = 1)
        self.columnconfigure(0, weight = 1)
        self.create_button_frame()
    
    
    def set_server_port_and_address(self, server_address, server_port) -> None:
        ''' Used to set the address/port of the server. '''
        self.server_address = server_address
        self.server_port = server_port

    def create_button_frame(self) -> None:
        ''' Assembles the components of the frame. '''
        button_lbl = Label(self, text = self.button_name, relief = GROOVE, bd = 2)
        button_lbl.grid(row = 0, column = 0, sticky = N+E+W+S)        
        
        action_button = Button(self, text = 'Send', command = lambda: self.send_to_server())
        action_button.grid(row = 1, column = 0, sticky = N+E+W+S)

    def send_to_server(self) -> None:
        ''' Opens a connection to the server, sends a message then closes the connection. Creates uses and then discards instance of SocketManager... I think. '''
        connection = SocketManager(address=self.server_address, port=self.server_port)
        t = Thread(target = connection.connect_and_send_msg_to_server,args = [self.button_send_text])
        t.start()

        # TODO Refactor, independent method being used instead of the one that every intance is using.        
        connection = None
        t = None



class ComplexButtonFrame(CommandButtonFrame):

    ''' Command Button frame that is used when multiple parameters require  user input.  '''

    def __init__(self, parent_frame, command_name: str, command_key: str, param_ls: list):
        super(ComplexButtonFrame, self).__init__(parent_frame)
        self.command_name: str = command_name
        self.command_key: str = command_key
        self.config(bd = 5)
        
        self.command_params: List[str] = param_ls
        self.inner_components: List = []

        self._assemble_frame()

    def _assemble_frame(self) -> None:
        self.columnconfigure(0, weight = 1)
        # Gonna use a 3 rows one column, First row for command name, second for another frame with all arguments, third for the send button.
        for i in range(3):
            self.rowconfigure(i, weight = 1)

        cmd_name = Label(self, text = self.command_name)
        cmd_name.grid(row = 0, column = 0, sticky = N+E+W+S)
        
        internal_frame = self._create_inner_command_frame()
        internal_frame.grid(row = 1, column = 0, sticky = N+E+W+S)

        send_btn = Button(self, text = 'Send', command = lambda: self.send_to_server(self.server_address, self.server_port, self._get_msg_to_send()))
        send_btn.grid(row = 2, column = 0, sticky = N+E+W+S)

    
    def _create_param_frame_with_text(self, parent_frame, lbl_name) -> List[Union[Frame, Label, Text]]:
        ''' Creates input fields with parameter passed. '''
        some_frame = Frame(parent_frame)
        some_frame.rowconfigure(0, weight = 1)
        some_frame.columnconfigure(0, weight = 1)
        some_frame.columnconfigure(1, weight = 1)
        lbl = Label(some_frame, text = lbl_name)
        lbl.grid(row = 0, column = 0, sticky = N+E+W+S)
        txt = Entry(some_frame, width = 5)
        txt.grid(row = 0, column = 1, sticky = N+E+W+S)
        return [some_frame, lbl, txt]

    def _get_msg_to_send(self) -> str:
        ''' Creates the message from the key given at creation and every parameter separated by ",". '''
        msg: str = self.command_key
        comp: Text
        for comp in self.inner_components:
            msg += ',' + comp.get()
        self.server_msg = msg
        return msg

    def _create_inner_command_frame(self) -> Frame:
        inner_frame = Frame(self)
        inner_frame.columnconfigure(0, weight = 1)
        inner_frame.columnconfigure(1, weight = 1)        
        current_row = 0
        for c_name in self.command_params:
            param_frame, param_lbl, param_txt = self._create_param_frame_with_text(inner_frame, c_name)            
            if current_row % 2 == 0:
                inner_frame.rowconfigure(current_row, weight = 1)
                param_frame.grid(row = current_row, column = 0, sticky = N+E+W+S)
                current_row += 1
                
            else:
                param_frame.grid(row = current_row - 1, column = 1, sticky = N+E+W+S)
                current_row += 1
            
            self.inner_components.append(param_txt)
        return inner_frame
            

        

# TODO Add a class so that the current server, port are visible and probably i could add the error label in too
class StatusFrame(TopWindow):
    ''' Not being used at the moment '''
    def __init__(self):
        pass


class SocketManager:   

    ''' Opens a socket to the specified address and port, sends a string to it and closes it. This is what is being used in the gui to send messages to the server. '''

    def __init__(self, address: str = DEFAULT_ADDRESS, port: int = DEFAULT_PORT):
        self.port = port
        self.address = address
        self.main_connection: socket = socket()
        self.main_connection.settimeout(2.0)
        self.connection_successful = False
        
    def connect_to_address(self) -> None:
        ''' Establish a connection to the server. '''        
        try:
            self.main_connection.connect((str(self.address), int(self.port)))
            self.connection_successful = True
        except timeout:
            print('Connection timed out.\nRemember to pass it later to the error label if it will be implemented.')
        except ConnectionRefusedError:
            print('Server appears to be down.')
        except OSError:
            print('Cannot connect to the server.')

    def send_message_to_address(self, msg: str) -> None:
        ''' Sends message to the server, then closes the connection. '''
        if self.connection_successful:
            self.main_connection.sendall(msg.encode())
            self.main_connection.close()

    def connect_and_send_msg_to_server(self, msg: str) -> None:
        ''' Connects establishes connection with the server, then sends message and closes it after. '''
        self.connect_to_address()
        self.send_message_to_address(msg)
        

class ScrollableFrame(Frame):

    ''' Frame that has a scrollable view and a scrollbar, currently its being used to display all the commands that will be sent to the server in the gui. '''

    def __init__(self, parent):
        super(ScrollableFrame, self).__init__(parent)        
        self.content_canvas = Canvas(self) 
        self.content_frame = Frame(self.content_canvas)
        self.frame_scroll_bar = Scrollbar(self, orient = 'vertical', command = self.content_canvas.yview)
        self.child_frame_ls: List[CommandButtonFrame] = []

        self._setup_components()

    def _setup_components(self):
        self.rowconfigure(0, weight = 1)
        self.columnconfigure(0, weight = 1)
        self.columnconfigure(1, weight = 0)
        
        self.content_frame.grid(row = 0, column = 0, sticky = N+E+W+S)
        self.content_frame.rowconfigure(0, weight = 1)
        self.content_frame.columnconfigure(0, weight = 1)
        self.content_canvas.grid(row = 0, column = 0, sticky = N+E+W+S)
        self.frame_scroll_bar.grid(row = 0, column = 1, sticky = N+S)       
               
        self.content_canvas.configure(yscrollcommand = self.frame_scroll_bar.set)        

        self.make_mouse_size_binding()

    def make_mouse_size_binding(self):
        '''' Sets the function for mousescroll in each operating system, mac hasn't been tested but who cares about mac users anyway.  ''' 
        self.content_canvas.create_window((0,0), window = self.content_frame, anchor = N+W)

        if platform == 'linux' or platform == 'linux2':
            #Checks if its linux.
            self.content_canvas.bind_all('<5>', lambda event: self._add_mouse_scroll_linux(event, increment = 1), add = '+')
            self.content_canvas.bind_all('<4>', lambda event: self._add_mouse_scroll_linux(event, increment = -1), add = '+')
        elif platform == 'darwin':
            #Checks if its mac
            self.content_canvas.bind_all('<MouseWheel>', self._mouse_scroll_mac)
        elif platform == 'win32':
            #Checks if its window
            self.content_canvas.bind_all('<MouseWheel>', self._mouse_scroll_windows)


        self.content_frame.bind('<Configure>', self.configure_canvas_size)

    def configure_canvas_size(self, event):
        ''' Sets the scroll view to frame that holds all the other command frames if any.  '''
        self.content_canvas.itemconfig('1', width = self.content_canvas.winfo_width())
        self.content_canvas.configure(scrollregion = self.content_canvas.bbox(ALL))

    def _add_mouse_scroll_linux(self, event, increment):
        self.content_canvas.yview_scroll(increment, 'units')
    
    def _mouse_scroll_windows(self, event):
        self.content_canvas.yview_scroll(int(-1*(event.delta/90)), 'units')

    def _mouse_scroll_mac(self, event):
        self.content_canvas.yview_scroll(int(-1*(event.delta)), 'units')

    def update_frame_addresses(self, address: str, port: str):
        ''' Refreshes the address of all the child command frames that it contains. '''
        command_frame: CommandButtonFrame
        for command_frame in self.child_frame_ls:
            command_frame.set_server_port_and_address(address, port)

    
    def _add_frame(self, someframe: Frame) -> None:        
        ''' Adds a command frame to the botton of the grid or scrollview, and then adds it to the internal list so that it can be modified from the parent. '''
        someframe.grid(row = len(self.child_frame_ls), column = 0, sticky = N+E+W+S)
        self.child_frame_ls.append(someframe)

    def add_simple_command(self, title: str, cmmd_key: str):
        ''' Creates and adds a simple command frame to the scroll view. '''
        command_frame = SimpleCommandButtonFrame(self.content_frame, title, cmmd_key)
        self._add_frame(command_frame)

    def add_param_command(self, title: str, cmmd_key: str, param_ls: List[str]):
        ''' Creates and adds a Command with multiple parameters and adds it to the scrollview. '''
        command_frame = ComplexButtonFrame(self.content_frame, title, cmmd_key, param_ls)
        self._add_frame(command_frame)

    def add_radio_command(self, title: str, option_dict: Dict[str, int], boolean_options: List[bool], lock: bool):
        ''' Creates a Complex Radio Boolean command frame and adds it to the scrollview. '''
        for key, val in option_dict.items():
            option_dict.pop(key)
            option_dict[key.lower()] = val
        command_frame = RadioButtonComplexFrame(title, self.content_frame)        
        command_frame.set_restrict_dictionary(option_dict)        
        command_frame.set_bool_options(boolean_options)
        command_frame.add_error_lbl()
        command_frame.auto_add_send_button()
        if lock:
            command_frame.lock_to_exact_param_number()
        self._add_frame(command_frame)     


class BooleanComboBoxWithTitle(Frame):

    ''' Creates a simple boolean dropdown frame with a label attached to it. '''

    def __init__(self, title, args):
        super(BooleanComboBoxWithTitle, self).__init__(args)
        self.title = title        
        self.rowconfigure(0, weight = 1)
        self.columnconfigure(0, weight = 1, minsize = 200)
        self.columnconfigure(1, weight = 1)
        self.configure(bd = 10, relief = GROOVE)
        self.current_str_val = StringVar(self)
        self.current_str_val.set('False')
        self.title_lbl = Label(self, text = self.title)
        self.title_lbl.grid(row = 0, column = 0, sticky = N+E+W+S)
        self.combo_box = Combobox(self, justify = 'center', state = 'readonly', values = ['False', 'True'], textvariable = self.current_str_val)        
        self.combo_box.grid(row = 0, column = 1, sticky = N+E+W+S)        

    def get_combo_box(self):
        return self.combo_box
    
    def get_current_value(self):
        return self.current_str_val.get()

    def set_trace_fnct(self, func):
        ''' Assigns a function to call each time the current value option changes. '''
        self.current_str_val.trace_add('write', func)

    

class RadioButtonComplexFrame(CommandButtonFrame):
    ''' Command frame to build commands that have multiple options and each option takes different parameters, can also add restriction to each option. '''

    def __init__(self, title, args):
        super(RadioButtonComplexFrame, self).__init__(args)
        self.title = title
        self.param_rest_dict = {}
        self.param_ls = []
        self.radios_ls = []
        self.correct_args_msg: bool = False
        self.lock_params: bool = False
        self.current_radio_option = StringVar(self)
        self.current_radio_option.trace_add('write', self.set_server_message)        
        self.radio_button_panel = Frame(self)
        self.radio_button_panel.columnconfigure(0, weight = 1)
        self.radio_button_panel.columnconfigure(1, weight = 1)
        self.radio_button_panel.grid(row = 1, column = 0, sticky = N+E+W+S)
        self.bool_param_panel = Frame(self)
        self.bool_param_panel.columnconfigure(0, weight = 1)
        self.bool_param_panel.columnconfigure(1, weight = 1)
        self.bool_param_panel.grid(row = 2, column = 0, sticky = N+E+W+S)
        self.title_lbl = Label(self, text = self.title)
        self.title_lbl.grid(row = 0 , column = 0, sticky = E+W)
        self.rowconfigure(0, weight = 1)
        self.rowconfigure(1, weight = 1)
        self.rowconfigure(2, weight = 1)        
        self.columnconfigure(0, weight = 1)   
        

    def _calculate_and_set_position_of_frame(self, frame_ls):
        # TODO Maybe this could be in another place because some other command frame could benefit from using this function instead of copying multiple times.
        ''' Finds the positon that the given frame will occupy and assigns it to the frame. This is used when options are being displayed in pairs. '''
        current_row_idx : int = len(frame_ls) - 1
        frame_ls: List[Frame]
        if current_row_idx % 2 == 0:
            a_frame: Frame
            a_frame = frame_ls[-1]
            a_frame.master.rowconfigure(current_row_idx, weight = 1)            
            a_frame.grid(row = current_row_idx, column = 0, sticky = N+E+W+S)
        else:
            a_frame: Frame
            a_frame = frame_ls[-1]
            a_frame.grid(row = current_row_idx - 1, column = 1, sticky = N+E+W+S)

    def add_param(self, bool_param_title: str):        
        ''' Creates a parameter frame and adds it to the internal list of parameters and assigns it to the scrollview. '''
        param_bool_frame = BooleanComboBoxWithTitle(bool_param_title, self.bool_param_panel)
        param_bool_frame.set_trace_fnct(self.set_server_message)
        self.param_ls.append(param_bool_frame)
        self._calculate_and_set_position_of_frame(self.param_ls)
        self.set_server_message() # This function here refreshes the value after an item is added
        
        

    def add_radio_option(self, radio_option_title: str):
        ''' Creates a radio option frame and adds it the internal list and assigns it to the scrollview. '''
        radio_option_frame = Radiobutton(self.radio_button_panel, text = radio_option_title, variable = self.current_radio_option, value = radio_option_title.lower(), bd = 2, relief = DEFAULT_BORDER)
        self.radios_ls.append(radio_option_frame)
        self._calculate_and_set_position_of_frame(self.radios_ls)
        self.set_server_message() # This function here refreshes the value after an item is added


    def set_restrict_dictionary(self, rest_dict: Dict[str, int]) -> None:
        ''' Takes a dictionary and assigns it to the current instance, then adds each option in the dicctionary to scrollable frame to be set up for display. '''
        self.param_rest_dict = rest_dict
        # Sets the initial value of the dictionary.
        if rest_dict and len(rest_dict) > 0:
            self.current_radio_option.set(list(rest_dict)[0])
            # Adds every radio option to the frame.
            for entry in rest_dict:
                self.add_radio_option(entry)
            

    def set_bool_options(self, options: List[bool]):
        ''' Takes a list of parameters and for each one it adds a boolean frame to the scrollview with each entry as a label. '''
        if options and len(options) > 0:
            for entry in options:
                self.add_param(entry)

    def _init_frame_radio_btns_bool_optns(self):        
        ''' Checks if any of the internal lists are empty, if not, then it proceeds to assemble everything inside the internal lists into the scrollable view. '''
        # print(self.param_ls)
        # print(self.radios_ls)        

        if self.param_ls == [] or self.radios_ls == []: # if any list is empty it reeschedules itself a second after to check again.
                self.after(100, self._init_frame_radio_btns_bool_optns)

        elif self.param_ls and len(self.param_ls) > 0:
            if self.radios_ls and len(self.radios_ls) > 0:
                for entry in self.param_ls:
                    self.add_param(entry)
                for entry in self.radios_ls: # I don't think this is running, why Good question.
                    self.add_radio_option(entry)

    def _check_restriction(self) -> bool:
        ''' Checks if the active parameters are as the restrictions indicate. '''
        max_options: int = int(self.param_rest_dict[self.current_radio_option.get()])
        counter: int = 0
        entry: BooleanComboBoxWithTitle
        for entry in self.param_ls:
            val = entry.get_current_value()
            if val == 'True':
                counter += 1
        if self.lock_params:
            if counter == max_options:
                return True    
        elif counter <= max_options:
            return True
        return False

        
    def set_server_message(self, *args):
        ''' Creates the message and assigns it as the message that will be sent to the server if the restrictions are met, otherwise it sets the message as bad. ''' 
        if self._check_restriction():
            msg: str = self.current_radio_option.get()     
            bool_box: BooleanComboBoxWithTitle
            for bool_box in self.param_ls:
                msg += f',{bool_box.get_current_value()}'            
            self.server_msg = msg
            self.correct_args_msg = True
        else:
            self.report_error(f'You can only select {self.param_rest_dict[self.current_radio_option.get()]} options as True')
            self.server_msg = self.set_bad_command_msg('Bad arguments!')
            self.correct_args_msg = False

    def lock_to_exact_param_number(self) -> bool:
        ''' Sets the falg to check for exact active commands or maximun active commands. '''
        # Dont call this before adding all the objects that will be present. (Call it last, after setting every component of the frame.)
        self.lock_params = not self.lock_params
        return self.lock_params
           
        
            
class ErrorLabel(Label):

    ''' Frame used to report errors that occur in the program as a graphic message through the use of a frame. '''
    # TODO Add a way to correctly handle multiple messages with a queue or something.
    def __init__(self, parent):
        super(ErrorLabel, self).__init__(parent)

    def set_message(self, message):
        self.config(text = message, fg = 'yellow', bg = 'red')
        self.after(3000, self.clear_message)

    def clear_message(self):
        self.config(text = '', bg = 'white', fg = 'black')


class CommandFrameCreator:

    ''' Reads a settings file and creates the command frames inside a scrollable frame. '''

    DEFAULT_BUTTON_CONFIG = 'resources/btn_config'    
    SIMPLE_COMMAND = 'simplecommand'    
    COMPLEX_PARAM_COMMAND = 'parametercommand'
    COMPLEX_BOOL_FRAME = 'complexboolframe'
    BOOL_OPTION = 'boolopt'
    LOCK_OPTIONS = 'lockboolopt'
    OPTION = 'option'
    COMMENT = '#'    


    SIMPLE_PARAM_REGEX = compile(r'[a-zA-Z0-9 ]+:[a-zA-Z0-9 ]+\n?')
    MULTIPLE_PARAM_REGEX = compile(r'([a-zA-Z0-9 ]+:?)+')
    PARAM_PAIR_LIMIT = compile(r'[a-zA-Z0-9 ]+: *[1-9][0-9 ]*\n?')

    def __init__(self, parent_frame: ScrollableFrame, config_file = DEFAULT_BUTTON_CONFIG):
        if not isinstance(parent_frame, ScrollableFrame):
            raise TypeError('Frame passed is not ScrollableFrame')
        self.config_filepath = config_file
        self.parent_frame: Frame = parent_frame
        self.file_contents: List[str] = read_file(self.config_filepath)
        
        self.current_line = -1

    def check_for_command(self):
        for _ in self.file_contents:          
            self._check_valid_command()            
        
    def clean_string_to_lower(self, somestring: str) -> str:
        return somestring.strip().lower()

    def _check_valid_command(self):
        self.current_line += 1
        if self.current_line >= len(self.file_contents):
            return
        txt_line = self.file_contents[self.current_line].strip().lower()                
        # print(repr(self.file_contents[self.current_line]), end = '--> ')
        # print(self.current_line)
        if txt_line and len(txt_line) > 0:            
            
            if txt_line == CommandFrameCreator.SIMPLE_COMMAND:
                self._simple_command()

            elif txt_line == CommandFrameCreator.COMPLEX_PARAM_COMMAND:
                self._param_command()

            elif txt_line == CommandFrameCreator.COMPLEX_BOOL_FRAME:
                self._complex_radio_command()

            elif txt_line[0] == CommandFrameCreator.COMMENT:
                self._comment_line()
    
    def _comment_line(self):
        ''' Do nothing. '''
        pass

    
    def _simple_command(self):
        command_name: str
        command_key: str
        current_txt_line = self._get_next_line()
        self._check_bad_syntax(current_txt_line, CommandFrameCreator.SIMPLE_PARAM_REGEX)
        command_name, command_key = current_txt_line.split(':')
        command_name = command_name.strip()
        command_key = command_key.strip()
        self.parent_frame.add_simple_command(command_name, command_key)
        self.current_line += 1        

    
    def _param_command(self):
        
        current_txt_line = self._get_next_line()
        self._check_bad_syntax(current_txt_line, CommandFrameCreator.SIMPLE_PARAM_REGEX)
        command_name: str
        command_key: str
        command_name, command_key = current_txt_line.split(':')
        current_txt_line = self._get_next_line()        
        self._check_bad_syntax(current_txt_line, CommandFrameCreator.MULTIPLE_PARAM_REGEX)
        current_txt_line: str = self.file_contents[self.current_line]
        param_ls = current_txt_line.split(':')
        for idx, param in enumerate(param_ls):
            param_ls[idx] = param.strip()
        self.parent_frame.add_param_command(command_name, command_key, param_ls)
        self.current_line += 1

    
    def _complex_radio_command(self):
        current_txt_line: str = self._get_next_line()
        self._check_bad_syntax(current_txt_line, compile(r'[A-Za-z0-9 ]+'))
        lock_options: bool = False
        command_name: str = current_txt_line.strip()
        current_txt_line = self._get_next_line()
        self._check_bad_syntax(self.clean_string_to_lower(current_txt_line), CommandFrameCreator.OPTION)
        current_txt_line = self._get_next_line()
        radio_opt_dict: dict = {}
        while True:
            self._check_bad_syntax(current_txt_line, CommandFrameCreator.PARAM_PAIR_LIMIT)
            option, limit = current_txt_line.split(':')
            radio_opt_dict[option.strip()] = int(limit.strip())
            current_txt_line = self._get_next_line()
            if self.clean_string_to_lower(current_txt_line.split(':')[0]) == CommandFrameCreator.BOOL_OPTION:            
                break
        
        bool_opt_ls = current_txt_line.split(':')[1:]
        for idx, opt in enumerate(bool_opt_ls):
            bool_opt_ls[idx] = opt.strip()
        try:
            current_txt_line = self._get_next_line()
            if self.clean_string_to_lower(current_txt_line) == CommandFrameCreator.LOCK_OPTIONS:
                lock_options = True
                current_txt_line = self._get_next_line()
        except IndexError:
            # Pichea
            print('Something bad happened.')            
        self.parent_frame.add_radio_command(command_name, radio_opt_dict, bool_opt_ls, lock_options)



    
    def _check_bad_syntax(self, some_txt: str, compiled_regex) -> None:        
        
        if isinstance(compiled_regex, Pattern) and not compiled_regex.fullmatch(some_txt):
            raise SyntaxError(f'Bad text format in:\n{some_txt}\n{repr(some_txt)}')

        elif not isinstance(compiled_regex, Pattern) and some_txt != compiled_regex:
            raise SyntaxError(f'Bad text format in:\n{some_txt}\n{repr(some_txt)}')
        

    def _get_next_line(self) -> str:
        self.current_line += 1
        return self.file_contents[self.current_line].strip()

if __name__=='__main__':
    ''' Starts the user interface. '''
    gui = CommandMenuGUI()
    

